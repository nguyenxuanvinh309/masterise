$(document).ready(function(){
	
	init();
	resizeAction();
	windowScroll();
	
});

function init() {
	sliderAction();
	resizeBackground();
}

function sliderAction() {
	var owl = $('.owl-carousel');

	owl.owlCarousel({
		items: 4,
		nav: true,
		loop: true,
		dots: false,
		margin: 0,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		navText: [
			"<img src='img/left-arrow.png' class='arrow-slider' />",
			"<img src='img/right-arrow.png' class='arrow-slider' />"
		],
		responsive: {
			0:{
				items:1
			},
			568:{
				items:2
			},
			600: {
				items:3
			},
			1000:{
				items:4
			}
		}
	});
}

function resizeAction(){
	$(window).resize(function(){
		resizeBackground();
	});
}

function resizeBackground(){
	var mainContentBck = $('.main-content-bck');

	if (mainContentBck.length > 0) {
		for (var i = 0; i < mainContentBck.length; i++) {
			mainContentBck.eq(i).css('height', $(window).height() - 1);
		}
	}
}

function backToTop() {
	$("html, body").animate({scrollTop: 0}, 1500);
}

function moveToBlock(id) {
	$('html, body').animate({
		scrollTop: $(`#${id}`).offset().top
	}, 1500);
}

function windowScroll() {
	$( window ).scroll(function() {
		if ($('.lang-desktop').length > 0) {
			if ($( window ).scrollTop() > 0) $('.lang-desktop').css('display','none');
			else $('.lang-desktop').css('display','block');
		}
	});
}
